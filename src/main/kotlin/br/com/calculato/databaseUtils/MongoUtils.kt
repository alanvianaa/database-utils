package br.com.calculato.databaseUtils

import org.bson.types.ObjectId
import java.time.LocalDate
import java.time.ZoneId

fun ObjectId.getLocalDate(): LocalDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()

