package br.com.calculato.databaseUtils

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table
import java.time.Instant
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

private val DEFAULT_TIME_STRING_FORMATTER by lazy {
    DateTimeFormatter.ISO_LOCAL_TIME.withLocale(Locale.ROOT).withZone(ZoneId.systemDefault())
}

class JavaLocalTimeColumnType : ColumnType() {
    override fun sqlType() = "TIME"

    override fun nonNullValueToString(value: Any): String {
        val localtime = when (value) {
            is String -> return value
            is LocalTime -> value
            is java.sql.Time -> value.toLocalTime()
            else -> error("Unexpected value: $value of ${value::class.qualifiedName}")
        }

        return "'${localtime.format(DEFAULT_TIME_STRING_FORMATTER)}'"
    }

    override fun valueFromDB(value: Any): Any = when (value) {
        is LocalTime -> value
        is java.sql.Time -> value.toLocalTime()
        is java.sql.Timestamp -> value.toLocalDateTime()
        is String -> LocalTime.parse(value)
        else -> valueFromDB(value.toString())
    }

    override fun notNullValueToDB(value: Any): Any {
        if (value is LocalTime) {
            return java.sql.Time.valueOf(value)
        }
        return value
    }

    companion object {
        internal val INSTANCE = JavaLocalTimeColumnType()
    }
}

fun Table.time(name: String): Column<LocalTime> = registerColumn(name, JavaLocalTimeColumnType())