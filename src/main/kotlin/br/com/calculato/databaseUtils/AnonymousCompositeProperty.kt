package br.com.calculato.databaseUtils

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Expression
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.QueryBuilder

/**
 * Expression to aggregate the [first] with the [second] columns of the composition
 */
class AnonymousCompositeExpression<L, R>(val first: Column<L>, val second: Column<R>) : Expression<Pair<L, R>>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) = queryBuilder {
        append("(${first.table.tableName}.${first.name}, ${second.table.tableName}.${second.name})")
    }
}

/**
 * Function to check if [this] composition is less or equal than the given [pair]
 */
infix fun <L, R> AnonymousCompositeExpression<out L, R>.lessEq(pair: Pair<L, R>): Op<Boolean> = CompositeEqOP(this, pair, CompositeEqOP.Operation.LESS_EQ)

/**
 * Function to check if [this] composition is greater or equal than the given [pair]
 */
infix fun <L, R> AnonymousCompositeExpression<out L, R>.greaterEq(pair: Pair<L, R>): Op<Boolean> = CompositeEqOP(this, pair, CompositeEqOP.Operation.GREATER_EQ)

/**
 * Aggregate [this] column to [another] column creating an anonymous composite type
 */
infix fun <L, R> Column<L>.compose(another: Column<R>) = AnonymousCompositeExpression(this, another)

/**
 * Creates a less or equals clause for [expr] columns against the given [pair]
 */
class CompositeEqOP<L, R>(val expr: AnonymousCompositeExpression<out L, R>, val pair: Pair<L, R>, val operation: Operation) : Op<Boolean>() {
    enum class Operation(val symbol: String) {
        LESS_EQ("<="),
        GREATER_EQ(">=")
    }

    override fun toQueryBuilder(queryBuilder: QueryBuilder) = queryBuilder {
        append("$expr ${operation.symbol} (")
        registerArgument(expr.first.columnType, pair.first)
        append(", ")
        registerArgument(expr.second.columnType, pair.second)
        append(")")
    }
}