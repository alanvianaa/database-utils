package br.com.calculato.databaseUtils

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.statements.jdbc.JdbcConnectionImpl
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.vendors.PostgreSQLDialect

/**
 * Text array column type to use with modern databases
 */
class TextArrayColumnType : ColumnType() {
    override fun sqlType(): String = buildString {
        if (TransactionManager.current().db.dialect is PostgreSQLDialect)
            append("TEXT ")
        append("ARRAY")
    }

    override fun valueToDB(value: Any?): Any? {
        if (value is Array<*>) {
            return (TransactionManager.currentOrNull()?.connection as JdbcConnectionImpl).connection.createArrayOf(
                "TEXT",
                value
            )
        } else {
            return super.valueToDB(value)
        }
    }

    override fun valueFromDB(value: Any): Any {
        if (value is java.sql.Array) return value.array
        if (value is Array<*>) return value

        error("Array does not support for this database")
    }
}

/**
 * Function to register custom TEXT ARRAY with the given [name]
 */
fun Table.textarray(name: String): Column<Array<String>> = registerColumn(name, TextArrayColumnType())