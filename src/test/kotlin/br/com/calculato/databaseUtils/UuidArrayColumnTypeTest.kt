package br.com.calculato.databaseUtils

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID

private object UuidTable : Table() {
    val id = integer("id")
    val coluna = uuidarray("coluna")
}

class UuidArrayColumnTypeTest : StringSpec({
    "uuidarray test" {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver", user = "root", password = "")

        transaction {
            SchemaUtils.create(UuidTable)

            val id = UuidTable.insert {
                it[id] = 1
                it[coluna] = arrayOf(
                        UUID.fromString("7be46512-e63c-458a-87e8-9bddd8dc96e1"),
                        UUID.fromString("9ac45d36-6c4e-4cdb-aad3-11920637f525"),
                        UUID.fromString("919bb034-1adf-40ad-a62c-7a1eb9e38249")
                )
            }[UuidTable.id]

            id shouldBe 1

            val coluna = UuidTable.select { UuidTable.id eq 1 }.single()[UuidTable.coluna]

            coluna shouldHaveSize 3
            coluna[0] shouldBe UUID.fromString("7be46512-e63c-458a-87e8-9bddd8dc96e1")
            coluna[1] shouldBe UUID.fromString("9ac45d36-6c4e-4cdb-aad3-11920637f525")
            coluna[2] shouldBe UUID.fromString("919bb034-1adf-40ad-a62c-7a1eb9e38249")
        }
    }
})
