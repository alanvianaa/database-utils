[![](https://jitpack.io/v/com.gitlab.calculato-oss/database-utils.svg)](https://jitpack.io/#com.gitlab.calculato-oss/database-utils)

![Squad calculato](https://img.shields.io/badge/squad-calculato-blue)
![dependency version](https://img.shields.io/badge/Kotlin_JVM-1.3.72-orange)
![dependency version](https://img.shields.io/badge/Kmongo-4.0.2-orange)
![dependency version](https://img.shields.io/badge/Exposed-0.25.1-orange)
# DataBase Utils

Biblioteca com complementos para ajudar no uso da biblioteca Exposed do kotlin.

## O que essa biblioteca faz?

* Funções para registros de colunas personalizadas (`integerarray`, `textarray`, `uuidarray`, `time`)
* Função que retorna data de registro do ObjectId
* Funções que compõem as requisições no Exposed

## Como usar

Passo 1: Adicione em repositories do arquivo build.gradle o jitpack.io:
```
repositories {
    maven { url 'https://jitpack.io' }
}
```
Passo 2: Adicione em dependencies
```
implementation "com.gitlab.calculato-oss:database-utils:$version"
```

[Usar em outras ferramentas de build](https://jitpack.io/v/com.gitlab.calculato-oss/database-utils)